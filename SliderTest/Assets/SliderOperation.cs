﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderOperation : MonoBehaviour
{
    public Slider slider;
    public Text Max;
    public Text Min;
    public Text Value;
    public InputField inputMax;
    public InputField inputMin;
    // Start is called before the first frame update
    void Start()
    {
        Max.text = slider.maxValue.ToString();
        Min.text = slider.minValue.ToString();
        Value.text = slider.value.ToString();

    }

    // Update is called once per frame
    void Update()
    {
        Max.text = slider.maxValue.ToString();
        Min.text = slider.minValue.ToString();
        Value.text = slider.value.ToString();
    }
    public void SetMax()
    {
        slider.maxValue = float.Parse(inputMax.text);
    }
    
    public void SetMin()
    {
        slider.minValue = float.Parse(inputMin.text);
    }

}
